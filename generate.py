from model import Decoder
from gptlike import Configuration, CharDataset
import torch
import time
from torch.utils.data import Dataset
from utils import tokens_to_string

config = Configuration()

# Construction of the training set
training_dataset = CharDataset(config,"shakespeare.txt")

# Get vocab_size from traning_dataset
config.vocab_size = training_dataset.get_vocab_size()

gptlike = Decoder(config)

# gptlike.load_state_dict(torch.load('littleModel3000BigMlp.pt'))
gptlike.load_state_dict(torch.load('littleModel200LittleMlp.pt'))

gptlike.eval()
start = time.time()
with torch.no_grad():
    context = "O God, O God!"
    # context = "je m'appelle Maxime, je suis etudiant a Geneve et j'adore les reseaux de neurones"
    # context = "Dear lady, how are u?"
    tokenized_context = torch.tensor([training_dataset.stoi[s] for s in context], dtype=torch.long)[None,...].to(config.device)
    # the model should implement a method to generate tokens given a prompt
    y = gptlike.generate(config, tokenized_context, 500, top_k=10)[0]
    completion = tokens_to_string(y, training_dataset)
gptlike.train()
resultat = ''.join(completion)
print(resultat)