import torch

def save_model(model, name):
    print("saving model")
    model_path = f"./{name}"
    torch.save(model.state_dict(), model_path)

def tokens_to_string(tokens, train_dataset):
    string = train_dataset.chars
    itos = [ string[tokens[i]] for i in range(len(tokens)) ]
    return itos

def write_on_file(name, data):
    chemin_fichier = f"miniprojet-2-language-model\{name}"

    # Ouvrez le fichier en mode écriture
    with open(chemin_fichier, "w") as fichier:
        # Écrivez le contenu de la variable dans le fichier
        for entier in data:
            fichier.write(str(entier) + " ")

def open_and_read(name):
    with open(name, "r") as fichier:
        contenu_fichier = fichier.read()

    contenu_fichier = contenu_fichier.split(" ")
    contenu_fichier = contenu_fichier[:-1]
    contenu_fichier = [float(element) for element in contenu_fichier]

    return contenu_fichier