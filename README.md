# Mini Project 2 - Language Model

## Training

gptlike.py

Execute this file by specifying the desired number of iterations (inside the training() function).

## Generation
generate.py

Execute this file by specifying the model in gptlike.load_state_dict(). It is also possible to customize the desired number of characters in this function.
