"""
Necessary classes for the implementation of our Transformer.
"""

import torch
import torch.nn as nn
import numpy as np
import math as mt
from torch.nn import functional as F

class SelfAttention(nn.Module):
    """
    NOT WORKING.

    Implementation of the multi-head masked causal self-attention.
    """

    def __init__(self, config):
        super().__init__()
        self.n_embd = config.n_embd
        self.n_head = config.n_head
        self.bloc_size = config.bloc_size
        self.device = config.device

        self.linear = nn.Linear(config.n_embd, 3*config.n_embd, device=config.device)
        self.linear2 = nn.Linear(config.n_embd, config.n_embd, device=config.device)
        
        self.dropout1 = nn.Dropout(0.2)
        self.dropout2 = nn.Dropout(0.2)

    def forward(self, setOfBatchs):
        B, N, n_embd = setOfBatchs.size()
        # Linear transformation: we triple the third dimension in setOfBatchs and assign n_embd to Q, K, and V 
        # Q.size = K.size = V.size =  (B, N, n_enbd)
        Q, K, V = self.linear(setOfBatchs).split(self.n_embd, dim=2)
        # "Creation" of heads, we re-size Q, K, and V along the third dimension
        # Q.size = K.size = V.size =  (B, N, n_head, n_embd/n_head)
        K = K.view(B, N, self.n_head, n_embd // self.n_head)
        Q = Q.view(B, N, self.n_head, n_embd // self.n_head)
        V = V.view(B, N, self.n_head, n_embd // self.n_head)
        # We transpose our dimensions to be able to perform the operation @
        Q = Q.transpose(1,2)
        K = K.transpose(1, 2).transpose(-2, -1)
        attention = Q @ K
        attention /= mt.sqrt(K.size(2))
        # Causal aspect
        attention = attention.masked_fill(torch.tril(torch.ones(self.bloc_size, self.bloc_size, device=self.device)).view(1, 1, self.bloc_size, self.bloc_size)[:,:,:N,:N] == 0, float('-inf'))
        # attention.size = (B, n_head, N, N)
        attention = F.softmax(attention, dim=1)
        attention = self.dropout1(attention)
        # We transpose the dimensions of V to enable @
        y = attention @ V.transpose(1,2)
        # attention.size = (B, n_head, N, n_embd/n_head)
        # We can now restore N to its original position
        y = y.transpose(1, 2)
        # attention.size = (B, N, n_head, n_embd/n_head)
        # Concatenation
        # attention.size = (B, N, n_embd)
        y = y.contiguous().view(B, N, n_embd)
        y = self.linear2(y)
        y = self.dropout2(y)
        return y

class DecoderBlock(nn.Module):
    """ 
    A decoder Block.
    """

    def __init__(self, config):
        super().__init__()
        self.ln_1 = nn.LayerNorm(config.n_embd, device=config.device)
        self.ln_2 = nn.LayerNorm(config.n_embd, device=config.device)

        self.attn = nn.MultiheadAttention(config.n_embd, config.n_head, batch_first=True, device=config.device)
        # self.attn = SelfAttention(config)

        self.mlp = nn.Sequential(
            nn.Linear(config.n_embd, 4* config.n_embd, device=config.device),
            nn.GELU(),
            nn.Linear(4 * config.n_embd, config.n_embd, device=config.device),
            nn.Dropout(0.2)
        )
        # self.mlp = nn.Linear(config.n_embd, config.n_embd,  device=config.device)

        self.device = config.device

    def forward(self, x, sizeOfBloc): 
        x_normed = self.ln_1(x)
        
        mask = (1 - torch.tril(torch.ones(sizeOfBloc, sizeOfBloc))).to(torch.bool).to(x.device)
        
        attn_output, _ = self.attn(x_normed, x_normed, x_normed, is_causal=True, attn_mask=mask)
        # attn_output = self.attn(x_normed)

        # x = x + SelfCausalAttention
        # resultat = x + mlp
        x = x + attn_output
        out = x + self.mlp(self.ln_2(x))
        return out

class Decoder(nn.Module):

    def __init__(self, config):
        super().__init__()
        self.bloc_size = config.bloc_size
        self.vocab_size = config.vocab_size

        self.wte = nn.Embedding(self.vocab_size, config.n_embd, device=config.device)
        self.dropout = nn.Dropout(0.2)
        self.layerNormFinal = nn.LayerNorm(config.n_embd, device=config.device)
        self.lm_head = nn.Linear(config.n_embd, config.vocab_size, device=config.device)
        self.layer = nn.ModuleList([DecoderBlock(config) for _ in range(config.n_layer)])

    def wpe(self, config, size):
        # print('WPE : ')
        pos_emb = torch.zeros((size[1], config.n_embd))
        for t in range(size[1]):
            tensor = torch.arange(0,config.n_embd)
            tensor[::2] = torch.cos(t/(  torch.pow(10000, 2*tensor[::2]/config.n_embd) ))
            tensor[1::2] = torch.sin(t/(  torch.pow(10000, 2*tensor[1::2]/config.n_embd) ))
            pos_emb[t] = tensor
        
        return pos_emb

    def forward(self, config, batch):

        size = batch.size()
        sizebloc = batch[0].size()[0]

        tok_emb = self.wte(batch)
        pos_emb = self.wpe(config, size)
        pos_emb = pos_emb.to(config.device)

        x = self.dropout(tok_emb+pos_emb)

        for bloc in self.layer:
            x = bloc(x, sizebloc)

        x = self.layerNormFinal(x)
        logits = self.lm_head(x)

        return logits

    @torch.no_grad()
    def generate(self, config, context, max, top_k):
        for _ in range(max):
            new_context = context if context.size(1) <= self.bloc_size else context[:, -self.bloc_size:]

            logits = self(config, new_context)

            # For all block, we considered last step
            logits = logits[:, -1, :]

            # k-largest element
            v, _ = torch.topk(logits, top_k)
            logits[logits < v[:, [-1]]] = -float('Inf')

            # apply softmax : probabilities
            probs = F.softmax(logits, dim=-1)

            # sampling according to probas
            next = torch.multinomial(probs, num_samples=1)

            context = torch.cat((context, next), dim=1)

        return context
 