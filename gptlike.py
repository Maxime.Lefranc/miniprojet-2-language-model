"""
Trains a character-level language model.
"""

import torch
import os
import torch.nn as nn
from torch.utils.data import Dataset
from torch.utils.data.dataloader import DataLoader
from torch.nn import functional as F
from model import Decoder
import time
from utils import save_model, write_on_file

class Configuration():
    """
    Configuration class containing key parameters.
    """
    def __init__(self):

        # data
        self.bloc_size = 128

        # model
        # self.n_layer = 12
        # self.n_embd = 768
        # self.n_head = 8
        self.n_layer = 6 
        self.n_embd = 192
        self.n_head = 6

        self.vocab_size = None

        # training
        self.learning_rate = 5e-4

        # Device
        self.device = torch.device('cuda')

class CharDataset(Dataset):
    """
    Emits batches of characters.

    Adapted from "https://github.com/karpathy/minGPT".
    """

    def __init__(self, config, data):
        txt = open(data, 'r').read()
        self.chars = sorted(list(set(txt)))
        self.config = config
        self.data = txt
        self.stoi = { ch:i for i,ch in enumerate(self.chars) } # map characters to integer indices
        self.vocab_size = len(self.chars)
        
    def get_vocab_size(self):
        return self.vocab_size

    def __len__(self):
        return len(self.data) - self.config.bloc_size
    
    def __getitem__(self, idx):
        # grab a chunk of (block_size + 1) characters from the data
        data_block = self.data[idx:idx + self.config.bloc_size + 1]

        # encode every character to an integer
        encoded_block = [self.stoi[c] for c in data_block]

        # return the chunk and the shifted version as tensors
        x = torch.Tensor(encoded_block[:-1]).to(torch.long)
        y = torch.Tensor(encoded_block[1:]).to(torch.long)

        return x, y
    
def training(config, model, train_dataset, nb_iters):

    model = model.to(config.device)
    model.train()
    all_loss = []
    all_times = []

    optimizer = torch.optim.Adam(model.parameters(), lr=config.learning_rate)

    loader = DataLoader(
            train_dataset,
            sampler=torch.utils.data.RandomSampler(train_dataset, replacement=True),
            shuffle=False,
            batch_size=128,
            num_workers=4,
        )
    
    iters = iter(loader)

    start = time.time()

    for i in range(nb_iters):
        print("i = ",i)
        datas = next(iters)

        datas = [t.to(config.device) for t in datas]
        batch, shift_batch = datas

        # Forward
        logits = model(config, batch)

        # loss 
        loss = F.cross_entropy(logits.view(-1, logits.size(-1)), shift_batch.view(-1), ignore_index=-1)
        all_loss.append(round(loss.item(), 2))
        all_times.append(time.time() - start)

        model.zero_grad(set_to_none=True) # Reset gradient
        loss.backward() # Backpropagation (Gradient Computation)
        optimizer.step() # MAJ parameters
    
    return all_loss, all_times

if __name__ == "__main__":
    print(torch.__version__)

    # Setup our configuration
    config = Configuration()
    
    # Construction of the training set
    training_dataset = CharDataset(config,"shakespeare.txt")

    # Get vocab_size from traning_dataset
    config.vocab_size = training_dataset.get_vocab_size()

    # Construction of the model
    gptlike = Decoder(config)

    # Train the model
    loss, exec_times = training(config, gptlike, training_dataset, 200)

    # save_model(gptlike, "LittleModel200LittleMlp.pt")

    # write_on_file("loss_LittleModel200LittleMlp.pt.txt", loss)
    # write_on_file("times_LittleModel200LittleMlp.pt.txt", exec_times)